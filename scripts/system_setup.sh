#!/bin/sh

EXTRA_PACKAGES="apg awesome awesome-extra emacs-lucid emacs-el \
                devscripts devscripts-el \
                elpa-company elpa-helm elpa-helm-projectile \
		elpa-ibuffer-projectile elpa-lua-mode elpa-markdown-mode elpa-nov \
		elpa-org-roam elpa-projectile elpa-rainbow-delimiters \
                elpa-rich-minority elpa-rust-mode elpa-smart-mode-line \
                emacs-goodies-el fonts-powerline fonts-inconsolata gitg \
	        golang-mode htop keepassx \
		magit markdown org-roam-doc pmount projectile-doc rust-all stgit stow \
		terminator tig tmux tmux-themepack-jimeh udiskie xcape xscreensaver \
                xscreensaver-screensaver-bsod zsh"

for package in ${EXTRA_PACKAGES}; do
    apt --yes install ${package}
done
