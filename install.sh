#!/bin/sh
# Simple script for installing configuration files
#
# For each file contained in config-files, creates a symbolic link to it in
# the user's home directory. Subdirectories are created as needed.

# Assume the configuration files to be installed are in the config-files directory at the
# same path where this script is called from.
BASE_DIR=`dirname $0`

#Skip version control directory and some documentations files.
IGNORED_REGEX="\.git.*|\.md|\.png|\.spec|~|#"

stow --target="${HOME}" --ignore="${IGNORED_REGEX}" --dir="${BASE_DIR}" config-files

for module in ${BASE_DIR}/gtp-*-config; do
    echo " ------------ Installing files from ${module} ------------ "
    ${module}/install.sh
done
